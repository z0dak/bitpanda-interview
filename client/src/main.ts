import VueCompositionAPI from '@vue/composition-api';
import '@fortawesome/fontawesome-free/css/all.css';
import '@fortawesome/fontawesome-free/js/all.js';
import axios from 'axios'
import VueAxios from 'vue-axios'
import Vue from 'vue';
import App from './App.vue';

Vue.use(VueAxios, axios)
Vue.config.productionTip = false;

Vue.use(VueCompositionAPI);

new Vue({
  render: (h) => h(App),
}).$mount('#app');
